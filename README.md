# RNode-cli

A javascript client to communicate with the RChain blockchain.

The [RChain Cooperative][1] is developing a decentralized, economically sustainable public compute infrastructure. Decentralized applications or "dApps" will run their business logic as smart contracts on the blockchain. Their userinterfaces will be more traditional programs that interact with the backend. This separation allows dApp developers to create nice abstract interfaces, and allows end users to create their own sovereign interfaces should they choose to do so.

[1]: https://www.rchain.coop/

## Real world meets blockchain
In the world of rholang, we facilitate [object capabilities](https://en.wikipedia.org/wiki/Object-capability_model) by using unforgeable names. These names exist only on the blockchain, and cannot be saved to disk or locked in a safe in the real world. This seems to create a problem when a human wants to use an unforgeable name to eg. update her facebook-style status, then later return to her computer and update it again. How does she keep track of the unforgeable name while away from her computer?

Public-key crypto to the rescue. The user can lock the relevant unforgeable name into "safe" a contract that anyone can call. When called the safe will give back the correct unforgeable name, but only if it is given a valid cryptographic signature. Michael Birch recently showed an [example of such a scheme](https://www.youtube.com/watch?v=WzAdfjwgaQs#t=9m28s).

## Quickstart
### Find an RChain node whose grpc you can use.
At the moment that likely means running your own rnode. We're working on a community node at rnode-test.rhobot.net

Make note of the hostname and gRPC port.

### Grab this code
Clone the repo with eg `git clone https://github.com/xxx`

An install dependencies with `yarn install`

## License
Copyright 2018 RChain Cooperative

Apache 2.0 License

See LICENSE.txt for details
